<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

//使用背包物品
Route::post('item/use', 'BackPackController@useItem');

//取得用戶背包物品列表
Route::get('items', 'BackPackController@getItemList');

/* 守護功能 */
Route::prefix('guardian')->group(function () {
    /* 取得權限 */
    Route::get('get_setting', 'GuardianController@getSetting');

    Route::middleware(['login_auth'])->group(function () {
        /* 我的守護資訊 */
        Route::get('my_info', 'GuardianController@myInfo');

        /* 開通紀錄 */
        Route::get('history', 'GuardianController@history');

        /*开通守护*/
        Route::post('buy', 'GuardianController@buy');

        /* 取得使用者消費紀錄 */
        Route::get('history', 'GuardianController@history');
    });
});
